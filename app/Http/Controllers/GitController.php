<?php

namespace App\Http\Controllers;

use App\GitWeb;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class GitController extends Controller
{
    public function index (Request $request){

        if(empty(env('URL_REP'))){
            echo 'No repository';
        }else{
            $gitweb = new GitWeb(env('URL_REP'));
        }

        if (isset($request['fetch'])){
            try {
                $gitweb->fetch($request['remote']);
                echo "Fetch successful \n";
            }catch (\Cz\Git\GitException $e){
                echo 'Error fetch' . $e;
            }
        }

        // gets list of all repository branches (remotes & locals)

        $branches = $gitweb->getBranches();

        if (isset($request['checkout'])){
            $branch = $request['branch'];
            if (strstr($branch,'remotes') == true){
                try {
                    $gitweb->checkout_remote(strstr($branch,'origin'));
                    echo "Checkout remotes\n";
                    $branches = $gitweb->getBranches();
                    $gitweb->fetch();
                }catch (\Cz\Git\GitException $e){
                    echo 'Error checkout remote' .$e;
                }

            }else{
                try {
                    $gitweb->checkout($branch);
                    echo "Checkout \n";
                } catch (\Cz\Git\GitException $e) {
                    echo 'Error checkout' . $e;
                }
            }
        }

        if (isset($request['pull'])){
            try {
                $gitweb->pull_branch($request['remote'], $gitweb->getCurrentBranchName());
                echo "All files are up-to-date \n";
            }catch (\Cz\Git\GitException $e){
                echo 'Error pull' . $e;
            }
        }

        $remotes = $gitweb->remote_v();

        return view('branched', ['gitweb' => $gitweb, 'remotes' => $remotes, 'branches' => $branches]);
    }
}
