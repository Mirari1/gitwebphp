<?php

namespace App;
use Cz\Git\GitRepository;

class GitWeb extends GitRepository
{
    function checkout_remote($name){
        return $this->begin()
            ->run("git checkout --track $name")
            ->end();
    }

    function remote_v(){
        return $this->extractFromCommand('git remote', function($value) {
            return trim($value);
        });
    }

    function pull_branch($remote = null, $branch = null){
        return $this->begin()
            ->run("git pull $remote $branch")
            ->end();
    }
}
