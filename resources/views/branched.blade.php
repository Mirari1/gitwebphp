<form method="post">
    @csrf
    <div>
        <select id="remotes" name="remote" >
            @foreach($remotes as $name_remote)
                <option value="{{$name_remote}} ">{{$name_remote}}</option>
            @endforeach
        </select>
    </div>
    <input type="submit" name="fetch" width="150px" value="fetch">
    <div>
        <select id="branches" name="branch" style="width: 150px" >
            @foreach($branches as $cc => $name)
                <option value="{{$name}}">{{$name}}</option>;
            @endforeach
        </select>
        Current branch: {{$gitweb->getCurrentBranchName()}}
        <input type="submit" name="checkout" width="150px" value="checkout">
    </div>
    <input type="submit" name="pull" onclick="" width="150px" value="pull">
</form>
<script>
    document.getElementById("branches").onchange = function() {
        localStorage.setItem('branches', document.getElementById("branches").value);
    };

    if (localStorage.getItem('branches')) {
        let savedValue = localStorage.getItem('branches');
        let option = document.querySelector('#branches > option[value="' + savedValue + '"]');
        if (option) {
            option.selected = true;
        }
    }

    document.getElementById("remotes").onchange = function() {
        localStorage.setItem('remotes', document.getElementById("remotes").value);
    };

    if (localStorage.getItem('remotes')) {
        let savedValue = localStorage.getItem('remotes');
        let option = document.querySelector('#remotes > option[value="' + savedValue + '"]');
        if (option) {
            option.selected = true;
        }
    }
</script>
